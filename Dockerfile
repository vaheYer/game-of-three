FROM adoptopenjdk/openjdk11
LABEL maintainer="vahe.yeritsyan.2001@gmail.com"
VOLUME /main-app
ADD target/GameOfThree-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar","/app.jar"]