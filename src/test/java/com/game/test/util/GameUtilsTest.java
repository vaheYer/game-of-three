package com.game.test.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GameUtilsTest {

    @Test
    void calculateMoveValue() {
        int moveValue = GameUtils.calculateMoveValue(10);
        assertEquals(1, GameUtils.calculateMoveValue(11));
        assertEquals(-1, GameUtils.calculateMoveValue(7));
        assertEquals(0, GameUtils.calculateMoveValue(99));
        assertThrows(IllegalArgumentException.class, () -> GameUtils.calculateMoveValue(-5));
    }
}