package com.game.test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.game.test.dto.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    private boolean finished;

    @Enumerated(value = EnumType.STRING)
    private Player winner;

    @OneToMany(mappedBy = "game", cascade = {CascadeType.ALL})
    @JsonIgnore
    private List<Move> moves;

    private Integer score;

    public void addMove(Move move) {
        if (moves == null) {
            moves = new ArrayList<>();
        }
        moves.add(move);
    }

}
