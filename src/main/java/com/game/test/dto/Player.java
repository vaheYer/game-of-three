package com.game.test.dto;

public enum Player {
    COMPUTER, USER
}