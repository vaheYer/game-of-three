package com.game.test.dto;

import com.game.test.model.Game;
import lombok.Data;
import lombok.Value;

@Data
@Value(staticConstructor = "from")
public class Response {
    private Game game;
    private MoveDto moveDto;
}