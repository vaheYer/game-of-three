package com.game.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoveDto {
    private Player player;

    @Range(min = -1, max = 1, message = "Number should be in range -1 to 1")
    private int move;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer moveFrom;


}
