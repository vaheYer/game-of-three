package com.game.test.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ApiError apiError = buildApiError(status, "Validation Error");
        apiError.addFieldErrors(ex.getBindingResult().getFieldErrors());
        apiError.addGlobalErrors(ex.getBindingResult().getGlobalErrors());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex) {
        ApiError apiError = buildApiError(BAD_REQUEST, "Validation Error");
        apiError.addConstraintViolations(ex.getConstraintViolations());
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler({
            IllegalArgumentException.class
    })
    public ResponseEntity<Object> handleIllegalArgument(RuntimeException ex) {
        return buildResponseEntity(buildApiError(BAD_REQUEST, ex));
    }

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<Object> handleIllegalState(IllegalStateException ex) {
        return buildResponseEntity(buildApiError(BAD_REQUEST, ex));
    }


    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> handleValidationException(ValidationException ex) {
        Exception cause = (Exception) ex.getCause();
        return buildResponseEntity(buildApiError(BAD_REQUEST, ex));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleGenericException(Exception ex) {
        return buildResponseEntity(buildApiError(INTERNAL_SERVER_ERROR, ex));
    }

    private ApiError buildApiError(@Nullable HttpStatus status, String message) {
        return ApiError.from(status != null ? status : INTERNAL_SERVER_ERROR, message);
    }

    private ApiError buildApiError(HttpStatus status, Exception ex) {
        return ApiError.from(status, ex.getMessage());
    }

    private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getHttpStatus());
    }
}
