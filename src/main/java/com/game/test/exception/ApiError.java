package com.game.test.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.validation.ConstraintViolation;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
public class ApiError {

    private final int code;
    private final String title;
    private String message = "No message available";

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private final LocalDateTime timestamp = LocalDateTime.now();


    private final Set<ErrorItem> errorItems = new HashSet<>();

    @JsonIgnore
    private final HttpStatus httpStatus;

    public static ApiError from(HttpStatus httpStatus, @Nullable String message) {
        ApiError apiError = new ApiError(httpStatus);
        apiError.setMessage(message);
        return apiError;
    }

    private ApiError(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        this.code = httpStatus.value();
        this.title = httpStatus.getReasonPhrase();
    }

    private void setMessage(String message) {
        if (StringUtils.hasText(message)) {
            this.message = message;
        }
    }


    public void addFieldErrors(List<FieldError> fieldErrors) {
        fieldErrors.stream()
                .map(ErrorItem::from)
                .forEach(errorItems::add);
    }

    public void addGlobalErrors(List<ObjectError> objectErrors) {
        objectErrors.stream()
                .map(ErrorItem::from)
                .forEach(errorItems::add);
    }

    public void addConstraintViolations(Set<ConstraintViolation<?>> constraintViolations) {
        constraintViolations.stream()
                .map(ErrorItem::from)
                .forEach(errorItems::add);
    }
}

