package com.game.test.service;

import com.game.test.dto.MoveDto;
import com.game.test.dto.Response;
import com.game.test.dto.StartGame;
import com.game.test.mapper.MoveMapper;
import com.game.test.model.Game;
import com.game.test.model.Move;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

import static com.game.test.dto.Player.COMPUTER;
import static com.game.test.dto.Player.USER;
import static com.game.test.util.GameUtils.calculateMoveValue;

@Service
@RequiredArgsConstructor
public class PlayService {
    private final GameService gameService;

    public Response startGame(StartGame startGame) {
        if (!startGame.isStartGame()) {
            return null;
        }
        List<Game> activeGames = gameService.getActiveGames();
        if (!(activeGames == null || activeGames.isEmpty())) {
            throw new IllegalArgumentException("There is already a game started");
        }
        Game game = new Game();
        int startNumber = new Random().nextInt(22) + 10;
        game.setScore(startNumber);
        if (startGame.getPlayer() == USER) {
            gameService.saveGame(game);
            return Response.from(game, null);
        } else {
            int moveValue = calculateMoveValue(startNumber);
            Move move = doMove(new MoveDto(COMPUTER, moveValue, startNumber), game);
            game.addMove(move);
            gameService.saveGame(game);
            return Response.from(game, MoveMapper.toDto(move));
        }
    }

    public Response play(MoveDto moveDto) {
        List<Game> activeGames = gameService.getActiveGames();
        if (activeGames == null || activeGames.isEmpty()) {
            throw new IllegalArgumentException("There are no active games started");
        }
        Game activeGame = activeGames.get(0);
        Move move = doMove(moveDto, activeGame);

        if (!activeGame.isFinished()) {
            int moveValue = calculateMoveValue(activeGame.getScore());
            move = doMove(new MoveDto(COMPUTER, moveValue, 0), activeGame);
            activeGame.addMove(move);
        }
        gameService.saveGame(activeGame);
        return Response.from(activeGame, MoveMapper.toDto(move));
    }

    private Move doMove(MoveDto moveDto, Game game) {
        int moveFrom = game.getScore();
        int sum = moveFrom + moveDto.getMove();
        if (sum % 3 != 0) {
            throw new IllegalArgumentException(sum + " is not divisible by three, please recalculate and try again");
        }
        int result = sum / 3;
        if (result == 1) {
            game.setWinner(moveDto.getPlayer());
            game.setFinished(true);
        }
        Move move = MoveMapper.toEntity(new MoveDto(moveDto.getPlayer(), moveDto.getMove(), moveDto.getMoveFrom()));
        move.setGame(game);
        game.setScore(result);
        return move;
    }


}