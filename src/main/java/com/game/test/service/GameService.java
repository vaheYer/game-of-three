package com.game.test.service;

import com.game.test.model.Game;
import com.game.test.repository.GameRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Getter
public class GameService {
    private final GameRepository gameRepository;

    public Game saveGame(Game game) {
        return gameRepository.save(game);
    }

    public List<Game> getActiveGames() {
        return gameRepository.getAllByFinishedFalse();
    }

}

