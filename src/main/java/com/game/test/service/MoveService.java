package com.game.test.service;

import com.game.test.model.Move;
import com.game.test.repository.MoveRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MoveService {
    private final MoveRepository moveRepository;

    public Move saveMove(Move move) {
        return moveRepository.save(move);
    }

}
