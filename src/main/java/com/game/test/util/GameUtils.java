package com.game.test.util;

public class GameUtils {
    public static int calculateMoveValue(Integer resultNumber) {
        if (resultNumber <= 1) {
            throw new IllegalArgumentException("Number should be bigger than 1");
        }
        int remainder = resultNumber % 3;
        if (remainder == 0) {
            return 0;
        }
        return remainder == 2 ? 1 : -1;
    }
}
