package com.game.test.mapper;


import com.game.test.dto.MoveDto;
import com.game.test.model.Move;

public class MoveMapper {
    public static Move toEntity(MoveDto moveDto) {
        Move move = new Move();
        move.setPlayer(moveDto.getPlayer());
        move.setMove(moveDto.getMove());
        move.setMoveFrom(moveDto.getMoveFrom());
        return move;
    }

    public static MoveDto toDto(Move move) {
        MoveDto moveDto = new MoveDto();
        moveDto.setPlayer(move.getPlayer());
        moveDto.setMove(move.getMove());
        moveDto.setMoveFrom(move.getMoveFrom());
        return moveDto;
    }
}
