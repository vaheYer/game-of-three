package com.game.test.controller;

import com.game.test.dto.MoveDto;
import com.game.test.dto.Response;
import com.game.test.dto.StartGame;
import com.game.test.service.PlayService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class GameController {
    private final PlayService playService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("start")
    public Response startGame(@RequestBody StartGame startGame) {
        return playService.startGame(startGame);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("play")
    public Response play(@RequestBody @Valid MoveDto moveDto) {
        return playService.play(moveDto);
    }

}